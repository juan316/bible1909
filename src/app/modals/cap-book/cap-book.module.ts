import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapBookComponent } from './cap-book.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [CapBookComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule

  ]
})
export class CapBookModule { }
