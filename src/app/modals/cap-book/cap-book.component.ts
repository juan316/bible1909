import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { JsonBibleService } from 'src/app/services/json-bible/json-bible.service';

@Component({
  selector: 'app-cap-book',
  templateUrl: './cap-book.component.html',
  styleUrls: ['./cap-book.component.scss'],
})
export class CapBookComponent  implements OnInit {

  constructor(
    private modalController: ModalController,
    private jsonService : JsonBibleService
  ) { }

  @Input() public name_book:any;

  ngOnInit() {
    let size=0;

    this.jsonService.getUniqueChaptersByBook(this.name_book).subscribe(
      verses => {
        console.log(verses)
        size=verses;
        for(let i = 1 ; i<= size; i++){
          this.listNumCap.push(i);
        }
      },
      error => {
        console.log(error);
      }
    );


    
  }


  listNumCap:any=[]

  async closeModal() {
    await this.modalController.dismiss({response:false});
  }

  async selectCap(val:any) {
    await this.modalController.dismiss({response:true,cap:val});
  }



}
