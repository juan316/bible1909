import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewContentPageRoutingModule } from './view-content-routing.module';

import { ViewContentPage } from './view-content.page';
import { CapBookModule } from 'src/app/modals/cap-book/cap-book.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewContentPageRoutingModule,
    CapBookModule 
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ViewContentPage]
})
export class ViewContentPageModule {}
