import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewContentPage } from './view-content.page';

describe('ViewContentPage', () => {
  let component: ViewContentPage;
  let fixture: ComponentFixture<ViewContentPage>;

  beforeEach(async() => {
    fixture = TestBed.createComponent(ViewContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
