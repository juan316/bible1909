import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent, IonSelect, ModalController } from '@ionic/angular';
import { CapBookComponent } from 'src/app/modals/cap-book/cap-book.component';
import { JsonBibleService } from 'src/app/services/json-bible/json-bible.service';

import Swiper from 'swiper';


@Component({
  selector: 'app-view-content',
  templateUrl: './view-content.page.html',
  styleUrls: ['./view-content.page.scss'],
})
export class ViewContentPage implements OnInit {

  constructor(
    public active: ActivatedRoute,
    private modalController: ModalController,
    private jsonService : JsonBibleService,
    private router: Router
    ) { }

    name_book:any;
    contentList:any=[];
    num_cap:any;
  

    @ViewChild('swiper') swiperRef: ElementRef | undefined;
    swiper?:Swiper;

  slidePreviewDestino=1;

    @ViewChild('content', { static: true }) content!: IonContent;

    scrollToTop() {
      this.content.scrollToTop(400); // 400ms de duración de la animación, opcional
    }


    swiperReady(){
      this.swiper = this.swiperRef?.nativeElement.swiper;
     
      this.swiper?.on('slideChange',()=>{
        this.chageSlide();
      })

    }
   
    chageSlide(){
      if (this.swiperRef) {
        // Acceder al elemento ViewChild y realizar la asignación
        this.swiperRef.nativeElement.style.height = 'auto'; // Por ejemplo, establece la altura en 500px
        this.scrollToTop();
      }
      setTimeout(() => {
        const elements = document.getElementsByClassName('swiper-slide-active');
        // Verificar si se encontraron elementos con la clase especificada
        if (elements.length > 0) {
          // Suponiendo que solo quieres la altura del primer elemento con la clase especificada
          const element :any = elements[0] as HTMLElement;
          // Obtener la altura del elemento
          const height = element.offsetHeight;
          if(height>0){
            //console.log('La altura del elemento es:', height);
            if (this.swiperRef) {
              // Acceder al elemento ViewChild y realizar la asignación
              this.swiperRef.nativeElement.style.height = height+'px'; // Por ejemplo, establece la altura en 500px
            }
          }
        }
      }, 1000);
    }

  ngOnInit() {
    let name_book = this.active.snapshot.paramMap.get('book');
    this.name_book = name_book?.toUpperCase();
    this.num_cap=1;
    this.loadBookByChaper();
  }

ngAfterViewInit(){
  this.chageSlide();  
}

  loadBookByChaper(){
    this.jsonService.getAllChaptersByBook(this.name_book).subscribe(
      verses => {
        //console.log(verses)
        this.contentList=verses;
      },
      error => {
        console.log(error);
      }
    );
  }

  goLibrary(){
    let section = this.active.snapshot.paramMap.get('section');
    this.clear();
    this.router.navigate(['/bible/books/'+section])
  }

  clear(){
    this.num_cap=1;
  }

  async openModal(){
    const modal = await this.modalController.create({
      component: CapBookComponent,
      componentProps: {
        'name_book': this.name_book
      }
    });
    modal.onWillDismiss().then(dataReturned => {
      console.log(dataReturned.data);
      let rsp = dataReturned.data;
      if(rsp.response){
        //this.num_cap=rsp.cap;
       // this.loadBookByChaper();
       let num = rsp.cap;
       this.swiperRef?.nativeElement.swiper.slideTo(Number(num)-1,10,false);
       // this.swiper?.slideTo(Number(num)-1,10,false);
      }
    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
      //console.log('Sending: ');
    });
  }



}
