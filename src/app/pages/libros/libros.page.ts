import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JsonBibleService } from 'src/app/services/json-bible/json-bible.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.page.html',
  styleUrls: ['./libros.page.scss'],
})
export class LibrosPage implements OnInit {

  constructor(private router: Router,
    private jsonService : JsonBibleService,
    public active: ActivatedRoute) {}



  listLibs:any=[];
  listLibsTemp:any=[];

  ngOnInit() {
    let section = this.active.snapshot.paramMap.get('section');

    this.jsonService.getUniqueChaptersBySection(section).subscribe(
      verses => {
        this.listLibs=verses;
        this.listLibsTemp=verses;
      },
      error => {
        console.log(error);
      }
    );
   
    
  }



  search(e: any) {
    const searchTerm = e.target.value.toLowerCase();
    if (searchTerm === '') {
      this.listLibs = this.listLibsTemp;
    } else {
      this.listLibs = this.listLibsTemp.filter((lib: any) => this.normalize(lib.nombre_libro.toLowerCase()).includes(this.normalize(searchTerm)));
    }
  }

  limpiarBusqueda() {
    // Lógica para manejar cuando se limpie la búsqueda
    this.listLibs = this.listLibsTemp;
  }

  goLibrary(book:any){
    let section = this.active.snapshot.paramMap.get('section');
    this.router.navigate(['/bible/book/'+section+'/'+book])
  }


  

  normalize(text: string): string {
    return text.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }

}
