import { TestBed } from '@angular/core/testing';

import { JsonBibleService } from './json-bible.service';

describe('JsonBibleService', () => {
  let service: JsonBibleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonBibleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
