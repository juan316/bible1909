import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JsonBibleService {
  private urlJson = "../assets/json/bible_json.json";

  constructor(private http: HttpClient) {}

  getBibleData(): Observable<any[]> {
    return this.http.get<any[]>(this.urlJson);
  }

  getVersesByBookAndChapter(bookName: any, chapter: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlJson).pipe(
      map((data: any[]) => {
        return data.filter(verse => 
          verse['nombre_libro']?.toLowerCase() === bookName?.toLowerCase() && verse['capitulo'] === chapter
        );
      })
    );
  }

  getAllVerses(): Observable<any[]> {
    return this.http.get<any[]>(this.urlJson);
  }

  getUniqueChaptersBySection(section: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlJson).pipe(
      map((data: any[]) => {
        const filteredData = data.filter(verse => verse['seccion']?.toLowerCase() === section);
        const chaptersByBook: { [bookName: string]: Set<number> } = {};
        filteredData.forEach(verse => {
          const bookName = verse['nombre_libro'];
          const chapter = verse['capitulo'];
          chaptersByBook[bookName] = chaptersByBook[bookName] || new Set<number>();
          chaptersByBook[bookName].add(chapter);
        });
        const uniqueChaptersByBook = Object.keys(chaptersByBook).map(bookName => ({
          'nombre_libro': bookName,
          'total_capitulos': chaptersByBook[bookName].size
        }));
        return uniqueChaptersByBook;
      })
    );
  }

  getAllChaptersByBook(bookName: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlJson).pipe(
      map((data: any[]) => {
        const chaptersByBook = data.filter(verse => 
          verse['nombre_libro']?.toLowerCase() === bookName?.toLowerCase()
        ).reduce((acc, curr) => {
          const chapter = curr['capitulo'];
          const verseInfo = { versiculo: curr['versiculo'], texto: curr['texto'] };
          acc[chapter] = acc[chapter] || { capitulo: chapter, nombre_libro: bookName, versiculos: [] };
          acc[chapter].versiculos.push(verseInfo);
          return acc;
        }, {});
        return Object.values(chaptersByBook);
      })
    );
  }

  getUniqueChaptersByBook(bookName: any): Observable<number> {
    return this.http.get<any[]>(this.urlJson).pipe(
      map((data: any[]) => {
        const filteredData = data.filter(verse => verse['nombre_libro']?.toLowerCase() === bookName?.toLowerCase());
        const uniqueChapters = new Set<number>();
        filteredData.forEach(verse => uniqueChapters.add(verse['capitulo']));
        return uniqueChapters.size;
      })
    );
  }

}
