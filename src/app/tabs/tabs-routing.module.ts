import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'bible',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'setting',
        loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
      },
      {
        path: '',
        redirectTo: '/bible/home',
        pathMatch: 'full'
      },
      {
        path: 'books/:section',
        loadChildren: () => import('../pages/libros/libros.module').then( m => m.LibrosPageModule)
      },
      {
        path: 'book/:section/:book',
        loadChildren: () => import('../pages/view-content/view-content.module').then( m => m.ViewContentPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/bible/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
